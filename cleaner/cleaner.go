package cleaner

import (
	"github.com/RadhiFadlillah/go-sastrawi"
)

var negationWords = sastrawi.NewDictionary("bukan", "enggak", "engga",
	"nggak", "ngga", "gak", "ga", "jangan", "jgn", "no", "tak", "tidak", "tdk")

var stopwords = sastrawi.NewDictionary("ada", "adalah", "adapun", "agak",
	"agar", "akan", "akhir", "aku", "amat", "anda", "antar", "antara",
	"apa", "apabila", "apakah", "apalagi", "apatah", "arti", "asal", "atas",
	"atau", "awal", "bagai", "bagaimana", "bagi", "bahkan", "bahwa",
	"bahwasanya", "baik", "bakal", "balik", "banyak", "bapak", "baru", "bawah",
	"beberapa", "begini", "begitu", "kerja", "belakang", "belum", "benar",
	"berapa", "berarti", "berbagai", "datang", "beri", "ikan", "ikut", "jumlah",
	"berkali", "kali", "kata", "hendak", "ingin", "berkenaan", "lain", "lalu",
	"langsung", "lebih", "macam", "maksud", "mula", "sama", "siap", "bertanya",
	"tanya", "turut", "tutur", "ujar", "upa", "besar", "betul", "biasa",
	"bila", "bisa", "boleh", "buat", "bukankah", "bulan", "bung", "cara",
	"cukup", "cuma", "dahulu", "dalam", "dan", "dapat", "dari", "daripada",
	"dekat", "demi", "demikian", "dengan", "depan", "di", "dia", "guna",
	"ibarat", "ingat", "jawab", "jelas", "karena", "diketahui", "tahu", "kira",
	"laku", "lihat", "minta", "misal", "mulai", "mungkin", "dini", "pasti",
	"perlu", "soal", "punya", "diri", "sampai", "sebut", "sini", "sin",
	"tambah", "tandas", "ditanya", "tegas", "tuju", "tunjuk", "ucap", "ungkap",
	"dong", "dua", "dulu", "empat", "entah", "hal", "hampir", "hanya", "hari",
	"harus", "hingga", "ia", "ialah", "ibu", "ini", "itu", "jadi", "jauh",
	"jika", "jikalau", "juga", "justru", "kala", "kalau", "kalaupun",
	"kalian", "kami", "kamu", "kan", "kapan", "kasus", "ke", "kecil", "lama",
	"lima", "keluar", "kembali", "kemudian", "kenapa", "kepada", "kesampaian",
	"seluruh", "terlalu", "ketika", "khusus", "kini", "kita", "kok", "kurang",
	"lagi", "lah", "lanjut", "lewat", "luar", "maka", "makanya", "makin",
	"malah", "mampu", "mana", "manakala", "manalagi", "masa", "masalah",
	"masih", "masing", "mau", "maupun", "memang", "memastikan", "memerlukan",
	"memihak", "meminta", "memintakan", "memisalkan", "memulai", "memungkinkan",
	"menaiki", "ambah", "menandaskan", "anti", "nanti", "mena", "menanyai",
	"menanyakan", "menegaskan", "mengapa", "mengatakan", "mengatakannya",
	"mengenai", "mengerjakan", "ira", "uju", "unjuk", "urut", "menuturkan",
	"sangkut", "menyatakan", "rasa", "mereka", "rupa", "meski", "yakin", "mirip",
	"nah", "naik", "namun", "nyaris", "nyata", "oleh", "pada", "padahal",
	"pak", "paling", "panjang", "pantas", "para", "penting", "per",
	"percuma", "pernah", "pertama", "tama", "pihak", "pukul", "pula", "pun",
	"rata", "saat", "saja", "saling", "sambil", "sana", "sangat", "satu",
	"saya", "se", "sebab", "sebagai", "sebagainya", "sebegini", "buah",
	"sedang", "sedemikian", "sedikit", "enak", "segala", "segera", "sehingga",
	"sejak", "jenak", "sejumlah", "sekadar", "sekali", "sekaligus",
	"sekalipun", "sekarang", "sekira", "sekitar", "sela", "selain", "selaku",
	"selalu", "semata", "mata", "sementara", "sempat", "semua", "sendiri",
	"seolah", "olah", "orang", "seperti", "sering", "serta", "sampa", "sekal",
	"seseorang", "sesuatu", "sudah", "setelah", "tempat", "tengah", "terus",
	"tiap", "tiba", "setinggi", "seusai", "waktu", "siapa", "suatu", "supaya",
	"tadi", "tahun", "tampak", "tanpa", "tapi", "telah", "tentang", "tentu",
	"tepat", "asa", "hadap", "terjadilah", "terjadinya", "terlebih", "masuk",
	"tersebut", "tersebutlah", "tertentu", "utama", "tetap", "tetapi", "tiga",
	"tinggi", "toh", "umum", "untuk", "usah", "usai", "waduh", "wah", "wahai",
	"walau", "walaupun", "wong", "yaitu", "yakni", "yang", "ya", "yg", "aja",
	"udah", "kalo", "nya", "kak", "iya", "gue", "tau", "sih", "ah", "banget",
	"nih", "yah", "gitu", "lo", "tp", "gua", "lg", "km", "ih", "lu", "emang",
	"tuh", "dah", "ku", "hai", "ni", "si", "udh", "yuk", "bgt", "eh", "min",
	"sm", "jd", "oh", "gk", "you", "jg", "loh", "klo", "gw", "ak", "nda",
	"dgn", "utk", "aj")

// Cleaner is object for stemming and tokenize document
type Cleaner struct {
	tokenizer sastrawi.Tokenizer
	stemmer   sastrawi.Stemmer
}

// NewCleaner returns new Cleaner
func NewCleaner() Cleaner {
	return Cleaner{
		tokenizer: sastrawi.NewTokenizer(),
		stemmer:   sastrawi.NewStemmer(sastrawi.DefaultDictionary),
	}
}

// Clean returns array of stemmed words from document
func (cleaner Cleaner) Clean(doc string) []string {
	// Create helper variable
	needNegation := false

	// Split, stem and remove stopword
	words := cleaner.tokenizer.Tokenize(doc)
	for i, word := range words {
		stem := cleaner.stemmer.Stem(word)

		// If empty, skip
		if stem == "" {
			continue
		}

		// If stopwords, skip
		if stopwords.Find(stem) {
			continue
		}

		// If negation, skip
		if negationWords.Find(stem) {
			needNegation = true
			continue
		}

		// If before is negation, add NOT
		if needNegation {
			stem = "NOT_" + stem
			needNegation = false
		}

		words[i] = stem
	}

	return words
}
