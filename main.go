//go:generate go-bindata -pkg handler -prefix view/ -ignore \.*\.scss -o handler/assets.go view/...
package main

import (
	"database/sql"
	"flag"
	"fmt"
	"net/http"
	"os"
	"os/exec"
	"runtime"
	"time"
)

var (
	flPortNumber = flag.Int("p", 8080, "Localhost port that used by application")
)

func main() {
	// Parse flags
	flag.Parse()

	// Create database directory
	os.Mkdir("./db", 0777)

	// Make sure training set database is exist
	if !isExist(pathToTrainingSet) {
		fmt.Println("Training set is not exist")
		os.Exit(1)
	}

	// Create and prepare back end
	backEnd := NewBackEnd()
	defer backEnd.Close()
	backEnd.PrepareBackEnd()

	// Start GUI
	go startGUI()

	// Serve app
	backEnd.ServeApp()
}

func startGUI() {
	// Create URL
	url := fmt.Sprintf("http://localhost:%d/", *flPortNumber)

	// Ping URL
	for {
		_, err := http.Get(url)
		if err == nil {
			break
		}

		time.Sleep(500 * time.Millisecond)
	}

	// Run GUI after ping success
	fmt.Println("Start GUI")
	cmd := "xdg-open"
	args := []string{url}
	if runtime.GOOS == "windows" {
		cmd = "cmd"
		args = []string{"/c", "start", url}
	}

	err := exec.Command(cmd, args...).Start()
	checkError(err)
}

func checkError(err error) {
	if err != nil && err != sql.ErrNoRows {
		panic(err)
	}
}
