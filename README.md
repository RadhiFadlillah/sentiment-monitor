Sentiment Monitor
=====================
Sentiment Monitor adalah aplikasi untuk memonitor sentimen masyarakat yang terkandung di dalam _tweet-tweet_ berbahasa Indonesia dengan _keyword_ tertentu, apakah sentimennya positif, negatif atau netral. Dikembangkan menggunakan bahasa pemrograman [Go](https://golang.org) untuk _back-end_ dan framework [Vue.js](https://vuejs.org/) untuk _front-end_.

Aplikasi ini dikembangkan untuk Tugas Akhir di Universitas Palangka Raya, serta telah diuji dan dipertahankan di depan penguji pada hari Senin, tanggal 6 Maret 2017, jam 08:00 - 09:30 WIB.

Screenshot
----------
![Screenshot](https://gitlab.com/RadhiFadlillah/sentiment-monitor/raw/master/readme/screenshot.png "Screenshot")

Cara Installasi
---------------
1. Download rilis terbaru dari halaman [_release_](https://gitlab.com/RadhiFadlillah/sentiment-monitor/tags) sesuai dengan platform yang digunakan.
2. Ekstrak hasil download ke folder yang diinginkan.
3. Di folder tersebut, buat folder baru dengan nama `db`.
4. Download database [training-set](https://gitlab.com/RadhiFadlillah/sentiment-monitor/blob/master/db/training-set.db), lalu letakkan di folder `db` yang telah dibuat sehingga struktur direktorinya menjadi sebagai berikut :

    ```
    sentiment-monitor*
    db/
    └── training-set.db
    ```

5. Aplikasi `sentiment-monitor` siap digunakan.

Cara Penggunaan Aplikasi
------------------------
1. Jalankan aplikasi `sentiment-monitor`. Jika di Linux, jalankan lewat `terminal`, jika di Windows bisa langsung klik dua kali.
2. Jika pertama kali digunakan, tunggu aplikasi melakukan _training_ terlebih dahulu. Setelah selesai, aplikasi akan menampilkan tampilan aplikasi di _browser_ default.
3. Login menggunakan akun Twitter.
4. Buat monitor baru.
5. Masukkan _keyword_ pencarian.
6. Tunggu aplikasi mengumpulkan _tweet_.
7. Lihat hasil _monitoring_.

Cara Kerja Aplikasi
-------------------
- Aplikasi melakukan klasifikasi sentimen pada _tweet_ menggunakan algoritma Naive-Bayes Classifier dengan model Multinomial Boolean dan ekstraksi fitur menggunakan unigram kata.
- Sebelum melakukan klasifikasi, dilakukan _preprocessing_ dengan menghapus link URL dan simbol-simbol pada _tweet_, dilanjutkan dengan melakukan _stemming_ menggunakan algoritma Confix-Stripping, sehingga didapatkan daftar kata dasar dalam _tweet_ tersebut.
- Data _training_ yang digunakan adalah 1.200.000 data dengan masing-masing 400.000 untuk setiap jenis sentimen. Pengumpulan data _training_ dilakukan  mengikuti definisi dari [penelitian](https://www-cs.stanford.edu/people/alecmgo/papers/TwitterDistantSupervision09.pdf) Go, Bhayani dan Huang (2009) :
  + Data _training_ positif dan negatif yang digunakan adalah _tweet-tweet_ yang mengandung _emoticon_. Jika _emoticon_ di _tweet_ tersebut adalah _emoticon_ senyum :) maka _tweet_ tersebut digunakan sebagai data _training_ positif, dan jika sebaliknya maka dianggap sebagai data _training_ negatif.
  + Data _training_ netral yang digunakan adalah semua dokumen yang dapat digunakan sebagai judul berita, sebab judul berita umumnya bersifat objektif. Daftar judul berita dikumpulkan dari situs [Kompas.com](http://www.kompas.com/), [Tempo.co](https://www.tempo.co/) dan [Tribunnews.com](http://www.tribunnews.com/).

Penjelasan lebih detail dapat dilihat di [laporan Tugas Akhir](https://gitlab.com/RadhiFadlillah/sentiment-monitor/raw/master/readme/laporan-akhir.pdf) yang telah saya buat (klik kanan, lalu pilih _Save-as_).

Kekurangan Aplikasi
-------------------
1. Aplikasi hanya dapat menganalisa _tweet_ yang berbahasa Indonesia saja, dan kurang akurat terhadap _tweet_ berbahasa daerah.
2. Aplikasi tidak mengklasifikan _tweet_ yang merupakan _retweet_ atau _replies_ dari _tweet_ lain.
3. Aplikasi tidak dapat mengklasifikasikan _tweet-tweet_ yang bersifat majas, kias, sarkasme atau sinisme secara akurat.
4. Aplikasi tidak dapat mengetahui konteks dari suatu _tweet_, sehingga sentimen _tweet_ hanya dinilai dari teksnya saja.
5. Dari [pengujian](https://gitlab.com/RadhiFadlillah/sentiment-monitor-test), aplikasi hanya dapat mengklasifikasikan _tweet_ dengan tingkat akurasi sebagai berikut :
    - Untuk sentimen netral, tingkat akurasi mencapai 97%.
    - Untuk sentimen positif, tingkat akurasi sekitar 78%.
    - Untuk sentimen negatif, tingkat akurasi sekitar 80%.
    - Secara keseluruhan, tingkat akurasi sekitar 85%.

_Library_ dan _Utility_
-----------------------
Dalam pengembangan aplikasi ini, digunakan beberapa _library_ dan _utility_, yaitu :

- [Go-Sastrawi](https://github.com/RadhiFadlillah/go-sastrawi) by [Radhi Fadlillah](https://github.com/RadhiFadlillah), digunakan untuk melakukan _stemming_.
- [Go-Bayesian](https://github.com/RadhiFadlillah/go-bayesian) by [Radhi Fadlillah](https://github.com/RadhiFadlillah), digunakan untuk melakukan klasifikasi menggunakan algoritma Naive-Bayes.
- [Anaconda](https://github.com/ChimeraCoder/anaconda) by [Aditya Mukerjee](https://github.com/ChimeraCoder), digunakan untuk mengakses Twitter API.
- [Go-OAuth](https://github.com/garyburd/go-oauth) by [Gary Burd](https://github.com/garyburd), digunakan untuk _client_ OAuth1.
- [Sqlx](https://github.com/jmoiron/sqlx) by [Jason Moiron](https://github.com/jmoiron), digunakan untuk mempermudah akses dan manipulasi database di bahasa Go.
- [HttpRouter](https://github.com/julienschmidt/httprouter) by [Julien Schmidt](https://github.com/julienschmidt), digunakan untuk melakukan _routing_ untuk setiap _request_ HTTP.
- [XLSX](https://github.com/tealeg/xlsx) by [Geoffrey J. Teale](https://github.com/tealeg), digunakan untuk mengekspor data menjadi file Excel.
- [Go-Sqlite3](https://github.com/mattn/go-sqlite3) by [Yasuhiro Matsumoto](https://github.com/mattn), digunakan sebagai _driver_ untuk mengakses database Sqlite3.
- [PB](https://github.com/cheggaaa/pb) by [Sergey Cherepanov](https://github.com/cheggaaa), digunakan untuk menampilkan _progress bar_.
- [Go-Bindata](https://github.com/jteeuwen/go-bindata) by [Jim Teuween](https://github.com/jteeuwen), digunakan untuk menggabung semua file ke dalam satu binary.

Lisensi
---------
*Source code* aplikasi ini disebar di bawah lisensi [GPLv3](http://choosealicense.com/licenses/gpl-3.0/) sehingga setiap orang dapat menggunakan, memodifikasi dan menyebarkannya, dengan syarat *souce code* tetap dibuka dan tetap dirilis di bawah lisensi GPLv3.

```
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
```