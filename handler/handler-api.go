package handler

import (
	"bufio"
	"database/sql"
	"encoding/json"
	"fmt"
	"html"
	"math"
	"net/http"
	"net/url"
	"os"
	"os/exec"
	"regexp"
	"runtime"
	"sentiment-monitor/model"
	"strconv"
	"strings"
	"time"

	"github.com/ChimeraCoder/anaconda"
	"github.com/RadhiFadlillah/go-bayesian"
	"github.com/garyburd/go-oauth/oauth"
	"github.com/jmoiron/sqlx"
	"github.com/julienschmidt/httprouter"
	"github.com/tealeg/xlsx"
)

// CreateMonitor is handler for POST /api/monitor.
// It saves monitor's data and queries to database.
func (handler *Handler) CreateMonitor(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	// Decode request
	var request model.NewMonitor
	checkError(json.NewDecoder(r.Body).Decode(&request))

	// Check monitor name
	if strings.TrimSpace(request.MonitorName) == "" {
		panic(fmt.Errorf("Monitor name is required"))
	}

	// Create regex for removing double space
	rxSpace := regexp.MustCompile(`\s+`)

	// Create function for setting parameter
	setParam := func(original, prefix, spaceChanger string) string {
		newQuery := prefix + strings.TrimSpace(original)
		newQuery = rxSpace.ReplaceAllString(newQuery, " ")
		newQuery = strings.Replace(newQuery, " ", spaceChanger, -1)
		return newQuery
	}

	// Create query
	filterQuery := ""

	// Add query for all words
	if strings.TrimSpace(request.AllWords) != "" {
		filterQuery += request.AllWords + " "
	}

	// Add query for exact phrase
	if strings.TrimSpace(request.ExactPhrase) != "" {
		filterQuery += `"` + request.ExactPhrase + `" `
	}

	// Add query for any of these words
	if strings.TrimSpace(request.AnyWords) != "" {
		filterQuery += setParam(request.AnyWords, "", " OR ") + " "
	}

	// Add query for none of these words
	if strings.TrimSpace(request.NoneWords) != "" {
		filterQuery += setParam(request.NoneWords, "-", " -") + " "
	}

	// Add query for hashtags
	if strings.TrimSpace(request.Hashtags) != "" {
		filterQuery += setParam(request.Hashtags, "#", " OR #") + " "
	}

	// Add query for since date
	if strings.TrimSpace(request.SinceDate) != "" {
		filterQuery += "since:" + request.SinceDate + " "
	}

	// Add query for until date
	if strings.TrimSpace(request.UntilDate) != "" {
		filterQuery += "until:" + request.UntilDate + " "
	}

	// Add query for excluding retweet, link and media
	filterQuery += "-filter:retweets -filter:replies -filter:links -filter:media"

	// Save it to database
	res := handler.db["main"].MustExec(
		`INSERT INTO monitor_list (name, query, all_words, exact_phrase,
		any_words, none_words, hashtags, since_date, until_date)
		VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)`,
		request.MonitorName,
		filterQuery,
		request.AllWords,
		request.ExactPhrase,
		request.AnyWords,
		request.NoneWords,
		request.Hashtags,
		request.SinceDate,
		request.UntilDate,
	)

	// Get last insert ID
	monitorID, err := res.LastInsertId()
	checkError(err)

	// Create database for new monitor
	strMonitorID := fmt.Sprintf("%d", monitorID)
	pathToDB := fmt.Sprintf("./db/monitor-%d.db", monitorID)
	handler.db[strMonitorID] = sqlx.MustConnect("sqlite3", pathToDB)

	// Create tables
	tx := handler.db[strMonitorID].MustBegin()

	tx.MustExec(
		`CREATE TABLE IF NOT EXISTS user (
		id          TEXT NOT NULL,
		name        TEXT NOT NULL,
		screen_name TEXT NOT NULL,
		PRIMARY KEY(id))`,
	)

	tx.MustExec(
		`CREATE TABLE IF NOT EXISTS tweet (
		id         TEXT NOT NULL,
		id_user    TEXT NOT NULL,
		content    TEXT NOT NULL,
		created    TEXT NOT NULL,
		sentiment  INTEGER NOT NULL DEFAULT 0,
		timestamp  TEXT NOT NULL DEFAULT CURRENT_TIMESTAMP,
		PRIMARY KEY(id),
		FOREIGN KEY(id_user) REFERENCES user(id))`,
	)

	checkError(tx.Commit())

	// Return success
	fmt.Fprint(w, monitorID)
}

// SelectMonitor is handler for GET /api/monitor.
// It fetch list of monitor in database and return it as JSON.
func (handler *Handler) SelectMonitor(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	// Get list monitor
	listMonitor := []model.Monitor{}
	err := handler.db["main"].Select(
		&listMonitor,
		"SELECT id, name FROM monitor_list ORDER BY name")
	checkError(err)

	// Return JSON
	w.Header().Add("Content-Type", "application/json")
	checkError(json.NewEncoder(w).Encode(listMonitor))
}

// DeleteMonitor is handler for DELETE /api/monitor/:id.
// It deletes monitor and his database based by its id.
func (handler *Handler) DeleteMonitor(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	// Get ID from url
	id := ps.ByName("id")

	// Remove monitor from main database
	handler.db["main"].MustExec("DELETE FROM monitor_list WHERE id = ?", id)

	// Disconnect monitor database
	err := handler.db[id].Close()
	checkError(err)

	// Delete database from map
	delete(handler.db, id)

	// Delete monitor database file
	err = os.Remove(fmt.Sprintf("./db/monitor-%s.db", id))
	checkError(err)

	fmt.Fprint(w, 1)
}

// GetMonitor is handler for GET /api/monitor/:id.
// It fetch monitor's summary and return it as JSON.
func (handler *Handler) GetMonitor(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	// Get monitor id from URL
	monitorID := ps.ByName("id")

	// Connect to specific monitor's database
	monitorDB := handler.db[monitorID]

	// Get monitor data
	monitor := model.Monitor{}
	err := handler.db["main"].Get(&monitor,
		`SELECT id, name, since_date, until_date 
		FROM monitor_list WHERE id = ?`, monitorID)

	// If monitor not exist, return 404
	if err == sql.ErrNoRows {
		http.Error(w, "Monitor is not exist", 404)
		return
	} else if err != nil {
		panic(err)
	}

	// Get monitor summary
	summary := model.MonitorSummary{}
	err = monitorDB.Get(
		&summary,
		`SELECT IFNULL(DATETIME(MIN(created)), '') start_time,
		IFNULL(DATETIME(MAX(created)), '') finish_time,
		IFNULL(COUNT(id), 0) n_tweet,
		IFNULL(SUM(sentiment = 1), 0) n_positive,
		IFNULL(SUM(sentiment = -1), 0) n_negative,
		IFNULL(SUM(sentiment = 0), 0) n_neutral FROM tweet`)
	checkError(err)

	// Get monitor graph data
	chart := []model.MonitorChartData{}
	err = monitorDB.Select(
		&chart,
		`SELECT DATE(created) date,
		SUM(sentiment = 1) n_positive,
		SUM(sentiment = -1) n_negative,
		SUM(sentiment = 0) n_neutral
		FROM tweet GROUP BY DATE(created)`)
	checkError(err)

	// Create result
	result := model.GetMonitorResult{
		Data:    monitor,
		Summary: summary,
		Chart:   chart,
	}

	// Return JSON
	w.Header().Add("Content-Type", "application/json")
	checkError(json.NewEncoder(w).Encode(result))
}

// FetchTweet is handler for GET /api/monitor/:id/fetch.
// It fetch tweets from API and save it to database.
func (handler *Handler) FetchTweet(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	// Get monitor id
	monitorID := ps.ByName("id")

	// Connect to monitor database
	monitorDB := handler.db[monitorID]

	// Get query for monitor
	var searchQuery string
	checkError(handler.db["main"].Get(
		&searchQuery,
		"SELECT query FROM monitor_list WHERE id = ?",
		monitorID,
	))

	// If there are no query, return
	if searchQuery == "" {
		fmt.Fprint(w, 0)
		return
	}

	// Set next max ID for Search API in Twitter
	var nextID string
	checkError(monitorDB.Get(&nextID, "SELECT IFNULL(MIN(id)-1, '') FROM tweet"))

	// Prepare search parameter
	searchParam := url.Values{}
	searchParam.Set("count", "100")
	searchParam.Set("result_type", "recent")
	searchParam.Set("lang", "id")

	if nextID != "" {
		searchParam.Set("max_id", nextID)
	}

	// Retrieve tweet from Twitter API
	searchResult, err := handler.twitterAPI.GetSearch(searchQuery, searchParam)
	checkError(err)

	// Begin transaction and prepare statement
	tx := monitorDB.MustBegin()

	stmtInsertUser, err := tx.Preparex(
		`INSERT OR IGNORE INTO user (id, name, screen_name)
		VALUES (?, ?, ?)`)
	checkError(err)

	stmtInsertTweet, err := tx.Preparex(
		`INSERT OR IGNORE INTO tweet (id, id_user, content, created, sentiment)
		VALUES (?, ?, ?, ?, ?)`)
	checkError(err)

	// Classify and save it to database
	for _, tweet := range searchResult.Statuses {
		tokens := handler.cleaner.Clean(tweet.Text)
		_, class, _ := handler.classifier.Classify(tokens...)

		var sentiment int
		if class == bayesian.Class("positive") {
			sentiment = 1
		} else if class == bayesian.Class("neutral") {
			sentiment = 0
		} else {
			sentiment = -1
		}

		tweetCreateTime, _ := tweet.CreatedAtTime()

		stmtInsertUser.MustExec(
			tweet.User.IdStr,
			tweet.User.Name,
			tweet.User.ScreenName,
		)

		stmtInsertTweet.MustExec(
			tweet.IdStr,
			tweet.User.IdStr,
			html.UnescapeString(tweet.Text),
			tweetCreateTime.Format("2006-01-02 15:04:05"),
			sentiment,
		)
	}

	// Commit
	checkError(tx.Commit())

	// Create fetch result
	var fetchResult model.FetchTweetResult
	err = monitorDB.Get(
		&fetchResult,
		`SELECT COUNT(id) 'tweet_count',
		IFNULL(MIN(created), '') 'from_time',
		IFNULL(MAX(created), '') 'until_time' FROM tweet`)
	checkError(err)

	fetchResult.FetchCount = len(searchResult.Statuses)

	// Return fetch result
	w.Header().Add("Content-Type", "application/json")
	checkError(json.NewEncoder(w).Encode(fetchResult))
}

// SelectTweet is handler for GET /api/monitor/:id/tweet.
// It fetch tweets from database and return it as JSON.
func (handler *Handler) SelectTweet(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	// Get monitor id
	monitorID := ps.ByName("id")

	// Connect to monitor database
	monitorDB := handler.db[monitorID]

	// Get filter param from URL
	urlQuery := r.URL.Query()
	page := urlQuery.Get("page")
	sinceDate := urlQuery.Get("from")
	untilDate := urlQuery.Get("to")
	_, showPositive := urlQuery["positive"]
	_, showNeutral := urlQuery["neutral"]
	_, showNegative := urlQuery["negative"]
	_, orderByLatest := urlQuery["latest"]

	// Get page number from URL
	pageNumber, err := strconv.Atoi(page)
	if err != nil {
		pageNumber = 1
	}

	// Create initial where clause and args
	var args []interface{}
	where := `WHERE 1`

	// Set where clause for since date
	if sinceDate != "" {
		where += ` AND DATE(t.created) >= ?`
		args = append(args, sinceDate)
	}

	// Set where clause for until date
	if untilDate != "" {
		where += ` AND DATE(t.created) <= ?`
		args = append(args, untilDate)
	}

	// Set where clause for show positive
	const andSentiment string = ` AND t.sentiment != ?`

	if !showPositive {
		where += andSentiment
		args = append(args, 1)
	}

	// Set where clause for show neutral
	if !showNeutral {
		where += andSentiment
		args = append(args, 0)
	}

	// Set where clause for show negative
	if !showNegative {
		where += andSentiment
		args = append(args, -1)
	}

	// Create query
	countQuery := `SELECT COUNT(t.id)
		FROM tweet t LEFT JOIN user u ON t.id_user = u.id ` + where

	selectQuery := `SELECT t.id, u.name, u.screen_name,
		t.content, t.created, t.sentiment
		FROM tweet t LEFT JOIN user u ON t.id_user = u.id ` + where

	// Set order clause
	selectQuery += ` ORDER BY t.id`
	if orderByLatest {
		selectQuery += ` DESC`
	}

	// Set limit clause
	selectQuery += fmt.Sprintf(" LIMIT 50 OFFSET %d", (pageNumber-1)*50)

	// Get count of tweet
	var count int
	checkError(monitorDB.Get(&count, countQuery, args...))

	// Calculate max page
	maxPage := int(math.Floor(float64(count)/50 + 0.5))

	// Get list of tweet
	listTweet := []model.Tweet{}
	checkError(monitorDB.Select(&listTweet, selectQuery, args...))

	// Create result
	result := model.SelectTweetResult{
		Page:    pageNumber,
		MaxPage: maxPage,
		Tweets:  listTweet,
	}

	// Return JSON
	w.Header().Add("Content-Type", "application/json")
	checkError(json.NewEncoder(w).Encode(result))
}

// Login is handler for POST /api/login.
// It generates URL and temp credentials for login in Twitter.
func (handler *Handler) Login(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	// Create auth url and temporary credentials
	authURL, tmpCred, err := anaconda.AuthorizationURL("oob")
	checkError(err)

	// Open URL in browser
	go func() {
		cmd := "xdg-open"
		args := []string{authURL}
		if runtime.GOOS == "windows" {
			cmd = "cmd"
			args = []string{"/c", "start", authURL}
		}

		err := exec.Command(cmd, args...).Start()
		checkError(err)
	}()

	// Encode credentials to JSON
	credentials := model.Credentials{
		Token:  tmpCred.Token,
		Secret: tmpCred.Secret,
	}

	// Send credentials
	w.Header().Add("Content-Type", "application/json")
	checkError(json.NewEncoder(w).Encode(credentials))
}

// VerifyLogin is handler for POST /api/login/verify.
// It generates Twitter credentials from verification code sent by user.
func (handler *Handler) VerifyLogin(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	// Decode request
	var request model.LoginVerificationRequest
	checkError(json.NewDecoder(r.Body).Decode(&request))

	// Create new temp credentials
	tmpCred := oauth.Credentials{
		Token:  request.TempCredentials.Token,
		Secret: request.TempCredentials.Secret,
	}

	// Get credentials
	cred, _, err := anaconda.GetCredentials(&tmpCred, request.VerificationCode)
	checkError(err)

	// Register Twitter API
	handler.twitterAPI = anaconda.NewTwitterApi(cred.Token, cred.Secret)
	handler.twitterAPI.EnableThrottling(5*time.Second, 5)

	// Get user data
	apiParam := url.Values{}
	apiParam.Set("include_entities", "false")
	apiParam.Set("skip_status", "false")
	user, err := handler.twitterAPI.GetSelf(apiParam)
	checkError(err)

	// Create login result
	loginResult := model.LoginResult{
		Token:      cred.Token,
		Secret:     cred.Secret,
		Name:       user.Name,
		ScreenName: user.ScreenName,
	}

	// Send result
	w.Header().Add("Content-Type", "application/json")
	checkError(json.NewEncoder(w).Encode(loginResult))
}

// RegisterTwitterToken is handler for POST /api/login/register.
// It accepts Twitter credetials and used it for next API calls.
func (handler *Handler) RegisterTwitterToken(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	// Decode request
	var request model.Credentials
	checkError(json.NewDecoder(r.Body).Decode(&request))

	// Register Twitter API
	handler.twitterAPI = anaconda.NewTwitterApi(request.Token, request.Secret)
	handler.twitterAPI.EnableThrottling(5*time.Second, 5)

	fmt.Fprint(w, 1)
}

// Exit is handler for POST /api/exit.
// It closes app completely.
func (handler *Handler) Exit(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	os.Exit(0)
}

// GetExcelFile is handler for GET /api/monitor/:id/xls.
// It create and serve Excel file a monitor.
func (handler *Handler) GetExcelFile(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	// Get monitor id from URL
	monitorID := ps.ByName("id")

	// Connect to specific monitor's database
	monitorDB := handler.db[monitorID]

	// Create writer, file and sheet
	writer := bufio.NewWriter(w)
	file := xlsx.NewFile()
	sheet, _ := file.AddSheet("tweets")

	// Select all tweet from database
	listTweet := []model.Tweet{}
	err := monitorDB.Select(&listTweet, `SELECT t.id, u.name, 
		u.screen_name, t.content, t.created, t.sentiment
		FROM tweet t LEFT JOIN user u ON t.id_user = u.id`)
	checkError(err)

	// Write table header
	row := sheet.AddRow()
	row.AddCell().Value = "No"
	row.AddCell().Value = "Name"
	row.AddCell().Value = "Username"
	row.AddCell().Value = "Time"
	row.AddCell().Value = "Tweet"
	row.AddCell().Value = "Sentiment"

	// Write table content
	for i, tweet := range listTweet {
		row = sheet.AddRow()
		row.AddCell().Value = strconv.Itoa(i + 1)
		row.AddCell().Value = tweet.Name
		row.AddCell().Value = tweet.ScreenName
		row.AddCell().Value = tweet.Created
		row.AddCell().Value = tweet.Content
		row.AddCell().Value = strconv.Itoa(tweet.Sentiment)
	}

	// Write file to buffer
	err = file.Write(writer)
	checkError(err)

	// Serve file
	w.Header().Add("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
	writer.Flush()
}
