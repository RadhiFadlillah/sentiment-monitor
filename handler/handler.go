package handler

import (
	"database/sql"
	"sentiment-monitor/cleaner"
	"time"

	"github.com/ChimeraCoder/anaconda"
	"github.com/RadhiFadlillah/go-bayesian"
	"github.com/jmoiron/sqlx"
)

// Handler represents handler for every API and routes.
type Handler struct {
	db         map[string]*sqlx.DB
	cleaner    cleaner.Cleaner
	classifier bayesian.Classifier
	twitterAPI *anaconda.TwitterApi
}

// NewHandler returns new Handler
func NewHandler(db map[string]*sqlx.DB, classifier bayesian.Classifier) *Handler {
	// Set Twitter API key
	anaconda.SetConsumerKey(consumerKey)
	anaconda.SetConsumerSecret(consumerSecret)

	// Create handler
	handler := new(Handler)
	handler.db = db
	handler.cleaner = cleaner.NewCleaner()
	handler.classifier = classifier
	handler.twitterAPI = anaconda.NewTwitterApi("", "")
	handler.twitterAPI.EnableThrottling(5*time.Second, 5)

	return handler
}

func checkError(err error) {
	if err != nil && err != sql.ErrNoRows {
		panic(err)
	}
}
