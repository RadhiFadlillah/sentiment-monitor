package model

// Monitor is container for monitor's data.
type Monitor struct {
	ID        int    `db:"id"         json:"id"`
	Name      string `db:"name"       json:"name"`
	Query     string `db:"query"      json:"-"`
	SinceDate string `db:"since_date" json:"sinceDate"`
	UntilDate string `db:"until_date" json:"untilDate"`
}

// MonitorChartData is container for monitor's chart data.
type MonitorChartData struct {
	Date      string `db:"date"       json:"date"`
	NPositive int    `db:"n_positive" json:"nPositive"`
	NNegative int    `db:"n_negative" json:"nNegative"`
	NNeutral  int    `db:"n_neutral"  json:"nNeutral"`
}

// MonitorSummary is container monitor's summary.
type MonitorSummary struct {
	StartTime  string `db:"start_time"  json:"startTime"`
	FinishTime string `db:"finish_time" json:"finishTime"`
	NTweet     int    `db:"n_tweet"     json:"nTweet"`
	NPositive  int    `db:"n_positive"  json:"nPositive"`
	NNegative  int    `db:"n_negative"  json:"nNegative"`
	NNeutral   int    `db:"n_neutral"   json:"nNeutral"`
}

// Tweet is container for tweet's data.
type Tweet struct {
	ID         string `db:"id"          json:"id"`
	Name       string `db:"name"        json:"name"`
	ScreenName string `db:"screen_name" json:"screenName"`
	Content    string `db:"content"     json:"content"`
	Created    string `db:"created"     json:"created"`
	Sentiment  int    `db:"sentiment"   json:"sentiment"`
}

// NewMonitor is request for new monitor.
type NewMonitor struct {
	MonitorName string `json:"monitorName"`
	AllWords    string `json:"allWords"`
	ExactPhrase string `json:"exactPhrase"`
	AnyWords    string `json:"anyWords"`
	NoneWords   string `json:"noneWords"`
	Hashtags    string `json:"hashtags"`
	SinceDate   string `json:"sinceDate"`
	UntilDate   string `json:"untilDate"`
}

// GetMonitorResult is respond for getting monitor from database.
type GetMonitorResult struct {
	Data    Monitor            `json:"data"`
	Summary MonitorSummary     `json:"summary"`
	Chart   []MonitorChartData `json:"chart"`
}

// FetchTweetResult is respond for fetching tweet from API.
type FetchTweetResult struct {
	FetchCount int    `db:"fetch_count" json:"fetchCount"`
	TweetCount int    `db:"tweet_count" json:"tweetCount"`
	FromTime   string `db:"from_time"   json:"fromTime"`
	UntilTime  string `db:"until_time"  json:"untilTime"`
}

// SelectTweetResult is respond for fetching tweet from database.
type SelectTweetResult struct {
	Page    int     `json:"page"`
	MaxPage int     `json:"maxPage"`
	Tweets  []Tweet `json:"tweets"`
}

// Credentials is container for Twitter's credential.
type Credentials struct {
	Token  string `json:"token"`
	Secret string `json:"secret"`
}

// LoginResult is container for login result.
type LoginResult struct {
	Token      string `json:"token"`
	Secret     string `json:"secret"`
	Name       string `json:"name"`
	ScreenName string `json:"screenName"`
}

// LoginVerificationRequest is container for login verification request.
type LoginVerificationRequest struct {
	VerificationCode string      `json:"verificationCode"`
	TempCredentials  Credentials `json:"tempCredentials"`
}
