package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"sentiment-monitor/cleaner"
	"sentiment-monitor/handler"

	"github.com/RadhiFadlillah/go-bayesian"
	"github.com/jmoiron/sqlx"
	"github.com/julienschmidt/httprouter"
	_ "github.com/mattn/go-sqlite3"
	"gopkg.in/cheggaaa/pb.v1"
)

const (
	pathToTrainingSet  string = "./db/training-set.db"
	pathToMainDatabase string = "./db/main.db"
	pathToClassifier   string = "./classifier"
)

// BackEnd is the back end of this app
type BackEnd struct {
	db         map[string]*sqlx.DB
	classifier bayesian.Classifier
}

// NewBackEnd returns new BackEnd
func NewBackEnd() *BackEnd {
	backEnd := new(BackEnd)
	backEnd.db = make(map[string]*sqlx.DB)
	return backEnd
}

// PrepareBackEnd prepares database and do Naive-Bayes training if needed
func (backend *BackEnd) PrepareBackEnd() {
	// Connect to database
	backend.db["main"] = sqlx.MustConnect("sqlite3", pathToMainDatabase)
	backend.db["trainingSet"] = sqlx.MustConnect("sqlite3", pathToTrainingSet)

	// Generate table for main database
	backend.db["main"].MustExec(
		`CREATE TABLE IF NOT EXISTS monitor_list (
		id                  INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
		name                TEXT NOT NULL UNIQUE,
		query               TEXT NOT NULL,
		all_words           TEXT NOT NULL,
		exact_phrase        TEXT NOT NULL,
		any_words           TEXT NOT NULL,
		none_words          TEXT NOT NULL,
		hashtags            TEXT NOT NULL,
		since_date          TEXT NOT NULL,
		until_date          TEXT NOT NULL)`,
	)

	// Create statement to delete monitor
	stmtDeleteMonitor, _ := backend.db["main"].Preparex("DELETE FROM monitor_list WHERE id = ?")
	defer stmtDeleteMonitor.Close()

	// Get list of all monitor ID
	var listMonitorID []int
	backend.db["main"].Select(&listMonitorID, "SELECT id FROM monitor_list ORDER BY id")

	// Create database connection for each monitor ID if it exist
	// and remove monitor ID if its database not exist
	for _, id := range listMonitorID {
		monitorID := fmt.Sprintf("%d", id)
		pathToDB := fmt.Sprintf("./db/monitor-%d.db", id)

		if isExist(pathToDB) {
			backend.db[monitorID] = sqlx.MustConnect("sqlite3", pathToDB)
		} else {
			stmtDeleteMonitor.MustExec(id)
		}
	}

	// Load classifier if it already exist,
	// else begin training and create a new classifier
	if isExist(pathToClassifier) {
		var err error
		backend.classifier, err = bayesian.NewClassifierFromFile(pathToClassifier)
		checkError(err)
	} else {
		// Get list of training set
		trainingSet := []struct {
			ID        int    `db:"id"`
			Tweet     string `db:"tweet"`
			Sentiment int    `db:"sentiment"`
		}{}

		err := backend.db["trainingSet"].Select(&trainingSet, "SELECT * FROM training_set")
		checkError(err)

		// Create classifier
		backend.classifier = bayesian.NewClassifier(bayesian.MultinomialBoolean)

		// Create cleaner
		docCleaner := cleaner.NewCleaner()

		// Define bayesian class
		classPositive := bayesian.Class("positive")
		classNegative := bayesian.Class("negative")
		classNeutral := bayesian.Class("neutral")

		// Begin training
		fmt.Println("Begin training")
		progress := pb.StartNew(len(trainingSet))

		for _, doc := range trainingSet {
			// Create map for storing unique word
			words := docCleaner.Clean(doc.Tweet)

			// If there are no words, skip
			if len(words) == 0 {
				continue
			}

			// Check document class
			var docClass bayesian.Class
			if doc.Sentiment == 1 {
				docClass = classPositive
			} else if doc.Sentiment == 0 {
				docClass = classNeutral
			} else {
				docClass = classNegative
			}

			// Do learning for this doc
			learningDoc := bayesian.NewDocument(docClass, words...)
			backend.classifier.Learn(learningDoc)

			// Increment progress bar
			progress.Increment()
		}

		// Save classifier to file
		err = backend.classifier.SaveClassifierToFile(pathToClassifier)
		checkError(err)
	}
}

// ServeApp runs router in port 8080
func (backend *BackEnd) ServeApp() {
	hdl := handler.NewHandler(backend.db, backend.classifier)
	router := httprouter.New()

	// Route to files
	router.GET("/", hdl.ServeIndexPage)
	router.GET("/fonts/*filepath", hdl.ServeFile)
	router.GET("/res/*filepath", hdl.ServeFile)
	router.GET("/css/*filepath", hdl.ServeFile)
	router.GET("/js/*filepath", hdl.ServeFile)

	// Route to APIs
	router.GET("/api/monitor", hdl.SelectMonitor)
	router.POST("/api/monitor", hdl.CreateMonitor)
	router.GET("/api/monitor/:id", hdl.GetMonitor)
	router.DELETE("/api/monitor/:id", hdl.DeleteMonitor)
	router.GET("/api/monitor/:id/fetch", hdl.FetchTweet)
	router.GET("/api/monitor/:id/tweet", hdl.SelectTweet)
	router.GET("/api/monitor/:id/xls", hdl.GetExcelFile)
	router.POST("/api/login", hdl.Login)
	router.POST("/api/login/verify", hdl.VerifyLogin)
	router.POST("/api/login/register", hdl.RegisterTwitterToken)
	router.POST("/api/exit", hdl.Exit)

	// Route for panic
	router.PanicHandler = func(w http.ResponseWriter, r *http.Request, arg interface{}) {
		http.Error(w, fmt.Sprint(arg), 500)
	}

	// Serve app
	url := fmt.Sprintf("localhost:%d", *flPortNumber)
	fmt.Println("Serve app in", url)
	log.Fatal(http.ListenAndServe(url, router))
}

// Close closes every database that connected to this app
func (backend *BackEnd) Close() {
	// Close database
	for key := range backend.db {
		backend.db[key].Close()
	}
}

func isExist(path string) bool {
	_, err := os.Stat(path)
	if err == nil {
		return true
	}

	return false
}
